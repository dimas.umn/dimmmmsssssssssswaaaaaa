import pymysql
import pymysql.cursors

from database_selection import *

def check_active(mobile):
    mobile = "{mob}@c.us".format(mob=mobile)
    try:
        driver.execute_script("window.WAPI.checkNumberStatus('" + mobile + "',function(done){ window.checkIsValid = done })")
    except Exception as checkError:
        logging.info(checkError)
    else:
        sleep(2)
    try:
        check = driver.execute_script("return window.checkIsValid")
    except Exception as checkError:
        logging.info(checkError)
    else:
        if check['status'] == 200:
            result = True
        else:
            result = False
    return result

def validate_phone(phone):
	result = 0
	try:
		if phone[:4] == '+628':
			result = 1
	except KeyboardInterrupt:
		print("Proses 'validate_phone' diberhentikan melalui KeyboardInterrupt.")
	except Exception:
		raise Exception
	return result

def get_processing(): #PER 100 PESAN; Q = 0; S = 0
	result = 0
	
	conn = pymysql.connect(host=DATABASE_HOST, user=DATABASE_USER, password=DATABASE_PASS, database=DATABASE_NAME,port=3306,
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
	try:
			with conn.cursor() as cursor:
				query = 'SELECT Id, MessageTo, MessageText FROM ' + TBL_OUT + ' WHERE IsQueued = 0 AND IsSent = 0 ORDER BY IsPriority DESC'
				cursor.execute(query)
				result = cursor.fetchall()
	except KeyboardInterrupt:
		print("Proses 'get_processing' diberhentikan melalui KeyboardInterrupt.")
	except Exception:
		raise Exception
	return result

def get_status(column, Id): 
	result = 0
	conn = pymysql.connect(host=DATABASE_HOST, user=DATABASE_USER, password=DATABASE_PASS, database=DATABASE_NAME,port=3306,
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
	try:
		with conn.cursor() as cursor:
				query = 'SELECT ' + str(column) + ' FROM ' + TBL_OUT + ' WHERE Id = ' + '\'' + str(Id) + '\''
				cursor.execute(query)
				for row in cursor:
					result = row['Id']
	except KeyboardInterrupt:
		print("Proses 'get_status' " + str(column) + " diberhentikan melalui KeyboardInterrupt.")
	except Exception:
		raise Exception
	return result

def get_status_queue(column, Id): 
	result = 0
	conn = pymysql.connect(host=DATABASE_HOST, user=DATABASE_USER, password=DATABASE_PASS, database=DATABASE_NAME,port=3306,
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
	try:
		with conn.cursor() as cursor:
				query = 'SELECT ' + str(column) + ' FROM ' + TBL_OUT + ' WHERE Id = ' + '\'' + str(Id) + '\''
				cursor.execute(query)
				for row in cursor:
					result = row['isQueued']
	except KeyboardInterrupt:
		print("Proses 'get_status' " + str(column) + " diberhentikan melalui KeyboardInterrupt.")
	except Exception:
		raise Exception
	return result	

def get_status_sent(column, Id): 
	result = 0
	conn = pymysql.connect(host=DATABASE_HOST, user=DATABASE_USER, password=DATABASE_PASS, database=DATABASE_NAME,port=3306,
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
	try:
		with conn.cursor() as cursor:
				query = 'SELECT ' + str(column) + ' FROM ' + TBL_OUT + ' WHERE Id = ' + '\'' + str(Id) + '\''
				cursor.execute(query)
				for row in cursor:
					result = row['isSent']
	except KeyboardInterrupt:
		print("Proses 'get_status' " + str(column) + " diberhentikan melalui KeyboardInterrupt.")
	except Exception:
		raise Exception
	return result	

def set_status(column, Id, value):
	result = 0
	conn = pymysql.connect(host=DATABASE_HOST, user=DATABASE_USER, password=DATABASE_PASS, database=DATABASE_NAME,port=3306,
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
	try:
		with conn.cursor() as cursor:
				query = 'UPDATE ' + TBL_OUT + ' SET ' + str(column) + ' = ' + str(value) + ' WHERE Id = ' + '\'' + str(Id) + '\''
				cursor.execute(query)
				conn.commit()
				result = 1
	except KeyboardInterrupt:
		print("Proses 'set_status' " + str(column) + " menjadi " + str(value) + "diberhentikan melalui KeyboardInterrupt.")
	except Exception:
		raise Exception
	return result

def reset_queue():
	result = 0
	conn = pymysql.connect(host=DATABASE_HOST, user=DATABASE_USER, password=DATABASE_PASS, database=DATABASE_NAME,port=3306,
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
	try:
		with conn.cursor() as cursor:
				query = 'UPDATE ' + TBL_OUT + ' SET IsQueued = 0 WHERE IsQueued = 1 AND IsSent = 0' #0 = unsent, 1 = sent
				cursor.execute(query)
				conn.commit()
				result = 1
	except KeyboardInterrupt:
		print("Proses 'reset_queue' diberhentikan melalui KeyboardInterrupt.")
	except Exception:
		raise Exception
	return result