import sys
import traceback
import time
import pymssql
import re
import datetime
import random

from webwhatsapi import WhatsAPIDriver
from webwhatsapi.objects.message import Message
from multiprocessing import Process, Lock, Queue

import functions_WAOut

def conn_whatsapp(lock, queue): #thrd_wa_send
	print("[" + functions_WAOut.WA_NAMA + "] Pembaca Whatsapp telah menyala.")
	try:
		lock.acquire()
		print("[" + functions_WAOut.WA_NAMA + "] Menyalakan driver...")
		driver = WhatsAPIDriver()
		print("[" + functions_WAOut.WA_NAMA + "] Menunggu hasil scan QR...")
		driver.wait_for_login()

		print("[" + functions_WAOut.WA_NAMA + "] Mereset antrian...")
		functions_WAOut.reset_queue()
		lock.release()

		while True:
			try:
				lock.acquire()
				print("[" + functions_WAOut.WA_NAMA + "] Mengecek antrian pesan...")
				if not queue.empty():
					try:
						print("[" + functions_WAOut.WA_NAMA + "] Mengecek pesan yang akan dikirim...")
						row = queue.get()

						while functions_WAOut.get_status('isSent',row[0]) == True:
							print("[" + functions_WAOut.WA_NAMA + "] Mengganti pesan yang akan dikirim...")
							row = queue.get()

						ID = row[0]
						recipient = row[1]
						content = row[2]

						print("[" + functions_WAOut.WA_NAMA + "] Mengirimkan pesan dari database...")
						time.sleep(random.randint(0,15))
						driver.send_message_to_id(recipient[1:] + "@c.us", content)
						print("[" + functions_WAOut.WA_NAMA + "] Pesan berisi '" + content + "' telah dikirim ke " + recipient + ".")
						functions_WAOut.set_status('isQueued', ID, 1)
						functions_WAOut.set_status('isSent', ID, 1)
					except Exception:
						functions_WAOut.set_status('isQueued', ID, 0)
						functions_WAOut.set_status('isSent', ID, 0)
				else:
					print("[" + functions_WAOut.WA_NAMA + "] Tidak ada antrian saat ini.")
			except KeyboardInterrupt:
				raise KeyboardInterrupt
				break
			except Exception:
				traceback.print_exc(file=sys.stdout)
				continue
			finally:					
				lock.release()
	except KeyboardInterrupt:
		print("[" + functions_WAOut.WA_NAMA + "] Proses 'conn_whatsapp' diberhentikan melalui KeyboardInterrupt.")
	except Exception:
		raise Exception
	finally:
		print("[" + functions_WAOut.WA_NAMA + "] Pembaca Whatsapp telah berhenti.")

def conn_db_out(lock, queue): #thrd_db_out
	print("[" + functions_WAOut.WA_NAMA + "] Pembaca Database telah menyala.")
	try:
		while True:
			try:
				lock.acquire()
				if queue.empty():
					print("[" + functions_WAOut.WA_NAMA + "] Mengecek pesan di database...")
					data = functions_WAOut.get_processing() #PER 100 PESAN; Q = 0; S = 0
					if len(data) == 0:
						print("[" + functions_WAOut.WA_NAMA + "] Tidak ada data baru saat ini.")
					else:
						for row in data:
							ID = row[0]
							phone = row[1]
							print("[" + functions_WAOut.WA_NAMA + "] Cek nomor valid...")
							if functions_WAOut.validate_phone(phone) == True:
								if functions_WAOut.get_status('isQueued',ID) == False:
									try:
										print("[" + functions_WAOut.WA_NAMA + "] Cek nomor aktif...")
										print("[" + functions_WAOut.WA_NAMA + "] Memasukkan data...")
										print(row)
										queue.put(row)
										functions_WAOut.set_status('isQueued', ID, 1)
									except Exception:
										functions_WAOut.set_status('isQueued', ID, 0)
							else:
								print("[" + functions_WAOut.WA_NAMA + "] Nomor tidak valid.")
								functions_WAOut.set_status('isSent', ID, 1)
			except KeyboardInterrupt:
				raise KeyboardInterrupt
				break
			except Exception:
				traceback.print_exc(file=sys.stdout)
				continue
			finally:
				lock.release()
				time.sleep(5)
	except KeyboardInterrupt:
		print("[" + functions_WAOut.WA_NAMA + "] Proses 'conn_db_out' diberhentikan melalui KeyboardInterrupt.")
	except Exception:
		raise Exception
	finally:
		print("[" + functions_WAOut.WA_NAMA + "] Pembaca Database telah berhenti.")

#-----------------------------#

def main():
	try:
		lock = Lock() #mastiin thread ga race condition
		queue = Queue()
		
		#thread
		print("[" + functions_WAOut.WA_NAMA + "] Menjalankan thread...")
		thrd_wa_send = Process(target=conn_whatsapp, args=(lock, queue))
		thrd_db_out = Process(target=conn_db_out, args=(lock, queue))
		thrd_wa_send.start()
		time.sleep(15)
		thrd_db_out.start()
		thrd_wa_send.join()
		thrd_db_out.join()
	except KeyboardInterrupt:
		print("[" + functions_WAOut.WA_NAMA + "] Proses 'main' diberhentikan melalui KeyboardInterrupt.")
	except Exception:
		traceback.print_exc(file=sys.stdout)
	finally:
		print("[" + functions_WAOut.WA_NAMA + "] Script telah berhenti.")
		sys.exit(0)

#-----------------------------#

if __name__ == "__main__":
	main()
