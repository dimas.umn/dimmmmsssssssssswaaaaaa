import sys
import traceback
import time
import pymysql
import pymysql.cursors
import re
import datetime
import random

from webwhatsapi import WhatsAPIDriver
from webwhatsapi.objects.message import Message
from multiprocessing import Process, Lock, Queue

import functions_WAOut

def conn_whatsapp(lock, queue): #thrd_wa_send
	print("[" + functions_WAOut.WA_NAMA + "] Pembaca Whatsapp telah menyala.")
	try:
		lock.acquire()
		print("[" + functions_WAOut.WA_NAMA + "] Menyalakan driver...")
		driver = WhatsAPIDriver(loadstyles=True)
		print("[" + functions_WAOut.WA_NAMA + "] Menunggu hasil scan QR...")
		driver.wait_for_login()

		print("[" + functions_WAOut.WA_NAMA + "] Mereset antrian...")
		functions_WAOut.reset_queue()
		lock.release()

		while True:
			for contact in driver.get_unread(include_notifications=True):
				for message in contact.messages:
					# print('class', message.__class__.__name__)
					# print('message', message)
					# print('id', message.id)
					# print('type', message.type)
					# print('timestamp', message.timestamp)
					# print('chat_id', message.chat_id)
					# print('sender', message.sender)

					try:
						sender_id = message.sender.id
					except:
						sender_id = 'NONE'
						# print('sender.id', sender_id)
						break

					try:
						sender_safe_name = message.sender.get_safe_name()
					except:
						sender_safe_name = 'NONE'
						# print('sender.safe_name', sender_safe_name)

					if message.type == 'chat':
						# print('-- Chat')
						# print('sender_id', sender_id)
#                                                print(message.safe_content.upper().find('HELO'))                                                
                                                if message.safe_content.upper().find('helo') >= 0 or message.safe_content.find('halo') >= 0 or message.safe_content.find('test') >= 0:
                                                    driver.send_message_to_id(sender_id, "BayarIniItu.com: Halo selamat datang. ketik *produk*. Untuk melihat layanan kami.")
                                                elif message.safe_content.upper().find('PRODUK') >= 0:
                                                    driver.send_message_to_id(sender_id, "BayarIniItu.com: Isi Pulsa, beli token PLN, top up ovo, gopay, dana, beli tiket pesawat, kirim wa blast bisa lewat kami loh.")                                                    
                                                else:
                                                    driver.send_message_to_id(sender_id, "BayarIniItu.com: Halo WA ini menggunakan robot. ketik *produk*. Untuk melihat layanan kami.")
#== 'halo...'.upper():
#                                                    elif
#                                                print('safe_content', message.safe_content)
#                                                driver.send_message_to_id(sender_id, "BayarIniItu.com: Jangan mengirimkan pesan ke nomor ini, karena nomor ini menggunakan robot.")
				break
				break

			try:
				lock.acquire()
				# print("[" + functions_WAOut.WA_NAMA + "] Mengecek antrian pesan...")
				if not queue.empty():
					try:
						print("[" + functions_WAOut.WA_NAMA + "] Mengecek pesan yang akan dikirim...")
						row = queue.get()
						print(row)

						while functions_WAOut.get_status_sent('isSent',row['Id']) == True:
							print("[" + functions_WAOut.WA_NAMA + "] Mengganti pesan yang akan dikirim...")
							row = queue.get()

							print(queue)

							print(row)
						ID = row['Id']
						recipient = row['MessageTo']
						content = row['MessageText']

						print("[" + functions_WAOut.WA_NAMA + "] Mengirimkan pesan dari database...")
						time.sleep(random.randint(0,15))
						driver.send_message_to_id(recipient[1:] + "@c.us", content)
						print("[" + functions_WAOut.WA_NAMA + "] Pesan berisi '" + content + "' telah dikirim ke " + recipient + ".")
						functions_WAOut.set_status('isQueued', ID, 1)
						functions_WAOut.set_status('isSent', ID, 1)
					except Exception:
						functions_WAOut.set_status('isQueued', ID, 0)
						functions_WAOut.set_status('isSent', ID, 0)
				# else:
					# print("[" + functions_WAOut.WA_NAMA + "] Tidak ada antrian saat ini.")
			except KeyboardInterrupt:
				raise KeyboardInterrupt
				break
			except Exception:
				traceback.print_exc(file=sys.stdout)
				continue
			finally:					
				lock.release()
	except KeyboardInterrupt:
		print("[" + functions_WAOut.WA_NAMA + "] Proses 'conn_whatsapp' diberhentikan melalui KeyboardInterrupt.")
	except Exception:
		raise Exception
	finally:
		print("[" + functions_WAOut.WA_NAMA + "] Pembaca Whatsapp telah berhenti.")

def conn_db_out(lock, queue): #thrd_db_out
	print("[" + functions_WAOut.WA_NAMA + "] Pembaca Database telah menyala.")
	try:
		while True:
			try:
				lock.acquire()
				if queue.empty():
					print("[" + functions_WAOut.WA_NAMA + "] Mengecek pesan di database...")
					data = functions_WAOut.get_processing() #PER 100 PESAN; Q = 0; S = 0
					
					# print(data) --> munculin data antrian

					print("ada "+str(len(data))+" antrian")
					if len(data) == 0:
						print("[" + functions_WAOut.WA_NAMA + "] Tidak ada data baru saat ini.")
					else:
						for row in data:
							ID = row['Id']
							phone = row['MessageTo']
							print("[" + functions_WAOut.WA_NAMA + "] Cek nomor valid...")
							if functions_WAOut.validate_phone(phone) == True:
								if functions_WAOut.get_status_queue('isQueued',ID) == False:
									try:
										print("[" + functions_WAOut.WA_NAMA + "] Cek nomor aktif...")
										print("[" + functions_WAOut.WA_NAMA + "] Memasukkan data...")
										# print(row)
										queue.put(row)
										functions_WAOut.set_status('isQueued', ID, 1)
									except Exception:
										functions_WAOut.set_status('isQueued', ID, 0)
							else:
								print("[" + functions_WAOut.WA_NAMA + "] Nomor tidak valid.")
								functions_WAOut.set_status('isSent', ID, 1)
			except KeyboardInterrupt:
				raise KeyboardInterrupt
				break
			except Exception:
				traceback.print_exc(file=sys.stdout)
				continue
			finally:
				lock.release()
				time.sleep(5)
	except KeyboardInterrupt:
		print("[" + functions_WAOut.WA_NAMA + "] Proses 'conn_db_out' diberhentikan melalui KeyboardInterrupt.")
	except Exception:
		raise Exception
	finally:
		print("[" + functions_WAOut.WA_NAMA + "] Pembaca Database telah berhenti.")

#-----------------------------#

def main():
	try:
		lock = Lock() #mastiin thread ga race condition
		queue = Queue()
		
		#thread
		print("[" + functions_WAOut.WA_NAMA + "] Menjalankan thread...")
		thrd_wa_send = Process(target=conn_whatsapp, args=(lock, queue))
		thrd_db_out = Process(target=conn_db_out, args=(lock, queue))
		thrd_wa_send.start()
		time.sleep(15)
		thrd_db_out.start()
		thrd_wa_send.join()
		thrd_db_out.join()
	except KeyboardInterrupt:
		print("[" + functions_WAOut.WA_NAMA + "] Proses 'main' diberhentikan melalui KeyboardInterrupt.")
	except Exception:
		traceback.print_exc(file=sys.stdout)
	finally:
		print("[" + functions_WAOut.WA_NAMA + "] Script telah berhenti.")
		sys.exit(0)

#-----------------------------#

if __name__ == "__main__":
	main()
